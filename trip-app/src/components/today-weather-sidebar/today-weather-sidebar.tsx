import { FC } from "react";

interface IProps {
  weekday: string;
  icon: string;
  temperature: string;
  city: string;
}
export const TodayWeatherSidebar: FC<IProps> = ({
  weekday,
  icon,
  temperature,
  city,
}) => {
  return (
    <div>
      <p className="sidebar-weekday">{weekday}</p>

      <div className="sidebar-icon">
        <img src={icon} />
        <span className="sidebar-temperature">
          {temperature}
          <span className="sidebar-temperature--celcium">
            <sup>°C</sup>
          </span>
        </span>
      </div>
      <p className="sidebar-city">{city}</p>
    </div>
  );
};
