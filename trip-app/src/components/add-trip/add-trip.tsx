import { FC } from "react";

interface IProps {
  onClick: () => void;
}

export const AddTripButton: FC<IProps> = ({ onClick }) => (
  <button className="add-trip-button" onClick={onClick}>
    <span className="add-trip-button__text plus">+</span>
    <p className="add-trip-button__text">Add Trip</p>
  </button>
);
