import { FC } from "react";

interface IProps {
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onSearch: () => void;
}

export const Search: FC<IProps> = ({ value, onChange, onSearch }) => {
  const handleSearchClick = () => {
    onSearch();
  };

  return (
    <div className="search-container">
      <button className="search-container__button" onClick={handleSearchClick}>
        <img
          className="search-container__icon"
          src="icons/search.svg"
          alt="Magnifying glass"
        />
      </button>
      <input
        className="search-container__input"
        type="search"
        placeholder="Search your trip"
        value={value}
        onChange={onChange}
      ></input>
    </div>
  );
};
