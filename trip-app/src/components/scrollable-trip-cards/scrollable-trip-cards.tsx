import { FC } from "react";
import { TripCard } from "./components/trip-card/trip-card";
import { Trip } from "../../types/trip";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";

interface IProps {
  trips: Trip[];
  onTripClick: (trip: Trip) => void;
}

export const ScrollableTripCards: FC<IProps> = ({ trips, onTripClick }) => {
  const slideLeft = () => {
    const slider = document.getElementById("slider");
    slider!.scrollLeft = slider!.scrollLeft - 350;
  };

  const slideRight = () => {
    const slider = document.getElementById("slider");
    slider!.scrollLeft = slider!.scrollLeft + 350;
  };

  const menuItems = trips.map((trip) => (
    <TripCard
      key={trip.id}
      id={trip.id}
      city={trip.city}
      image={trip.image}
      startDate={trip.startDate}
      endDate={trip.endDate}
      onClick={() => onTripClick(trip)}
    />
  ));

  return (
    <div className="relative flex items-center max-w-[860px] mr-[15px]">
      <MdChevronLeft onClick={slideLeft} size={40} />
      <div
        id="slider"
        className="w-full h-full overflow-x-scroll scroll whitespace-nowrap scroll-smooth"
      >
        {menuItems}
      </div>
      <MdChevronRight onClick={slideRight} size={40} />
    </div>
  );
};
