import { FC } from "react";

interface IProps {
  id: string;
  city: string;
  image: string;
  startDate: string;
  endDate: string;
  onClick: () => void;
}

export const TripCard: FC<IProps> = ({
  city,
  image,
  startDate,
  endDate,
  onClick,
}) => {
  return (
    <div className="trip-card-container" onClick={onClick}>
      <img className="trip-card-container__image" src={image} alt="city" />

      <p className="trip-card-container__city-title">{city}</p>
      <p className="trip-card-container__dates">
        {startDate} - {endDate}
      </p>
    </div>
  );
};
