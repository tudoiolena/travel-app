export type Trip = {
  id: string;
  city: string;
  image: string;
  startDate: string;
  endDate: string;
};
