import { ForecastResponse } from "../interfaces/forecast.response";
import { TodayWeatherResponse } from "../interfaces/today-weather.response";

const API_KEY = "UNL7AC4HP3WPGY3XU5GYV2G9B";

const baseURL =
  "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline";

const apiCall = async <T>(url: string): Promise<T> => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  } catch (err) {
    console.error("Error fetching weather forecast: ", err);
    throw new Error("Failed to fetch data");
  }
};

export const fetchWeatherPeriodForecast = async (
  city: string,
  startDate: string,
  endDate: string
): Promise<ForecastResponse> => {
  const url = `${baseURL}/${city}/${startDate}/${endDate}?unitGroup=metric&include=days&key=${API_KEY}&contentType=json`;
  return apiCall<ForecastResponse>(url);
};

export const fetchWeatherTodayForecast = async (
  city: string
): Promise<TodayWeatherResponse> => {
  const url = `${baseURL}/${city}/today?unitGroup=metric&include=days&key=${API_KEY}&contentType=json`;
  return apiCall<TodayWeatherResponse>(url);
};
